const state = () => ({
    systemInfo:{
        name:'记账本',
        version:'1.20210105'
    }
})

// getters
const getters = {
   
}

// actions
const actions = {
    
}

// mutations
const mutations = {
   
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}