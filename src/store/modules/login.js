const state = () => ({
    isLogin: false,
    userId: '',
    loginInfo:{},
    wxInfo:{},
    redirectFullPath:''
})

// getters
const getters = {
    
}

// actions
const actions = {
    
}

// mutations
const mutations = {
    setRedirectFullPath (state, redirectFullPath){
       state.redirectFullPath = redirectFullPath;
    },
    setUserId (state, userId){
       state.userId = userId;
    },
    setLoginInfo (state, loginInfo){
       state.loginInfo = loginInfo;
    },
    setWxInfo (state, wxInfo){
       state.wxInfo = wxInfo;
    },
    setIsLogin(state,login){
        state.isLogin = login;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}