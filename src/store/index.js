import Vue from 'vue'
import Vuex from 'vuex'
import application from './modules/application'
import login from './modules/login'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    application,
    login
  }
})