import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from 'axios'
import VueAxios from 'vue-axios'
import { Dialog } from 'vant';
import { Loading } from 'vant';
import { Overlay } from 'vant';

Vue.config.productionTip = false

import LicenseKeyboard from 'vue-license-keyboard';
import 'vue-license-keyboard/lib/vue-license-keyboard.css';

import 'vant/lib/dialog/style';
import 'vant/lib/loading/style';
import 'vant/lib/overlay/style';

Vue.use(LicenseKeyboard);
Vue.use(Dialog);
Vue.use(Loading);
Vue.use(Overlay);


axios.defaults.baseURL = 'http://192.168.1.3:9004/api';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.get['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
// axios.defaults.transformRequest = [function (data) {
//     var newData = "";
//     for (var k in data) {
//         newData += encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) + '&'
//     }
//     return newData
// }]
axios.interceptors.request.use(config => {
	// 为请求头添加Authorization字段为服务端返回的token
	
	return config
  })

//全局守卫
router.beforeEach((to, from, next)=> {
  
		if (to.meta.title) {
			document.title = to.meta.title;
		}
		var accessToken = sessionStorage.getItem('access_token');
		
		if(to.meta.isLogin){
			if(accessToken == null){
				next("/login");
			}
		}

		next();
  }
);

Vue.use(VueAxios,axios);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
