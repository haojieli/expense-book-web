import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [

  
  {
    path: '/login',
    component: () => import('../views/login.vue'),
    meta:{title:'数据加载中...',isLogin:false}
  },
  {
    path: '/expense',
    component: () => import('../views/expense.vue'),
    meta:{title:'记账本',isLogin:true},
    children: [
      {
        path: 'form',
        component: () => import('../views/expense/form.vue'),
        meta:{title:'记一笔', isLogin:true}
      },
      {
        path: 'book',
        component: () => import('../views/expense/book.vue'),
        meta:{title:'我的记账本',isLogin:true}
      },
      {
        path: 'book/detail',
        component: () => import('../views/expense/detail.vue'),
        meta:{title:'记账本详情',isLogin:true}
      },
      {
        path: 'book/info',
        component: () => import('../views/expense/Info.vue'),
        meta:{title:'账单详情',isLogin:true}
      }
      
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
